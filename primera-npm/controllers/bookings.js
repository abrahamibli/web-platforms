const express = require('express');
const { Booking } = require('../db');

function list(req, res, next) {
    res.send('GET /directors/ => list');
}

function index(req, res, next) {
    res.send('GET /directors/:id => index');
}

function create(req, res, next) {
    let booking = new Object();
    booking.date = req.body.date;
    
    Booking.create(booking).then(booking => res.status(200).json({
        message: "Booking creado correctamente",
        objs: booking
    })).catch(error => {
        //res.status(500);
        console.error(error);
    });
}

function replace(req, res, next) {
    res.send('PATCH /directors/ => replace');
}

function update(req, res, next) {
    res.send('PUT /directors/ => update');
}

function destroy(req, res, next) {
    res.send('DELETE /directors/ => destroy');
}

module.exports = {
    list, index, create, replace, update, destroy
}
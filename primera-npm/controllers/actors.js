const express = require('express');
const { Actor } = require('../db');

function list(req, res, next) {
    res.send('GET /actors/ => list');
}

function index(req, res, next) {
    res.send('GET /directors/:id => index');
}

function create(req, res, next) {
    let actor = new Object();
    actor.name = req.body.name;
    actor.lastName = req.body.lastName;

    Actor.create(actor).then(actor => res.status(200).json({
        message: "Actor creado correctamente",
        objs: actor
    })).catch(error => {
        //res.status(500);
        console.error(error);
    });
}

function replace(req, res, next) {
    res.send('PATCH /directors/ => replace');
}

function update(req, res, next) {
    res.send('PUT /directors/ => update');
}

function destroy(req, res, next) {
    res.send('DELETE /directors/ => destroy');
}

module.exports = {
    list, index, create, replace, update, destroy
}
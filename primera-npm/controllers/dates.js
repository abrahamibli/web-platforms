const express = require('express');

function getDate(req, res, next) {
    let d = new Date();
    res.send(`Fecha actual: ${d.getDate()}`);
}

module.exports = {
    getDate
}
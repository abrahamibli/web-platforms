const express = require('express');

function list(req, res, next) {
    res.send('GET /users/ => list');
}

function index(req, res, next) {
    const result = parseInt(req.params.id) + parseInt(req.params.id);
    //res.send('GET /users/:id => index');
    res.send(`Res X2: ${result}`);
}

function create(req, res, next) {
    res.send('POST /users/ => create');
}

function replace(req, res, next) {
    res.send('PATCH /users/ => replace');
}

function update(req, res, next) {
    res.send('PUT /users/ => update');
}

function destroy(req, res, next) {
    res.send('DELETE /users/ => destroy');
}

module.exports = {
    list, index, create, replace, update, destroy
}
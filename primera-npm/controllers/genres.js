const express = require('express');
const { Genre } = require('../db');

function list(req, res, next) {
    res.send('GET /directors/ => list');
}

function index(req, res, next) {
    res.send('GET /directors/:id => index');
}

function create(req, res, next) {
    let genre = new Object();
    genre.description = req.body.description;
    genre.status = req.body.status;

    Genre.create(genre).then(genre => res.status(200).json({
        message: "Genero creado correctamente",
        objs: genre
    })).catch(error => {
        //res.status(500);
        console.error(error);
    });
}

function replace(req, res, next) {
    res.send('PATCH /directors/ => replace');
}

function update(req, res, next) {
    res.send('PUT /directors/ => update');
}

function destroy(req, res, next) {
    res.send('DELETE /directors/ => destroy');
}

module.exports = {
    list, index, create, replace, update, destroy
}
const express = require('express');
const { Member } = require('../db');

function list(req, res, next) {
    res.send('GET /directors/ => list');
}

function index(req, res, next) {
    res.send('GET /directors/:id => index');
}

function create(req, res, next) {
    let member = new Object();
    member.name = req.body.name;
    member.lastName = req.body.lastName;
    member.address = req.body.address;
    member.phone = req.body.phone;
    member.status = req.body.status;

    Member.create(member).then(member => res.status(200).json({
        message: "Miembro creado correctamente",
        objs: member
    })).catch(error => {
        //res.status(500);
        console.error(error);
    });
}

function replace(req, res, next) {
    res.send('PATCH /directors/ => replace');
}

function update(req, res, next) {
    res.send('PUT /directors/ => update');
}

function destroy(req, res, next) {
    res.send('DELETE /directors/ => destroy');
}

module.exports = {
    list, index, create, replace, update, destroy
}
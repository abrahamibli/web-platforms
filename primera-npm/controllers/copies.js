const express = require('express');
const { Copy } = require('../db');

function list(req, res, next) {
    res.send('GET /directors/ => list');
}

function index(req, res, next) {
    res.send('GET /directors/:id => index');
}

function create(req, res, next) {
    let copy = new Object();
    copy.number = req.body.number;
    copy.format = req.body.format;
    copy.status = req.body.status;

    Copy.create(copy).then(copy => res.status(200).json({
        message: "Director creado correctamente",
        objs: copy
    })).catch(error => {
        //res.status(500);
        console.error(error);
    });
}

function replace(req, res, next) {
    res.send('PATCH /directors/ => replace');
}

function update(req, res, next) {
    res.send('PUT /directors/ => update');
}

function destroy(req, res, next) {
    res.send('DELETE /directors/ => destroy');
}

module.exports = {
    list, index, create, replace, update, destroy
}
module.exports = (sequelize, type) => {
    const Copy = sequelize.define('copy', {
        id: {type: type.INTEGER, primaryKey: true, autoIncrement: true},
        number: {type: type.STRING, notNull: true},
        format: type.ENUM('Digital'),
        status: type.ENUM('hola')
    });

    return Copy;
}
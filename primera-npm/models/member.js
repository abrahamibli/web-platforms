module.exports = (sequelize, type) => {
    const Member = sequelize.define('member', {
        id: {type: type.INTEGER, primaryKey: true, autoIncrement: true},
        name: {type: type.STRING, notNull: true},
        lastName: type.STRING,
        address: type.STRING,
        phone: type.STRING,
        status: type.INTEGER
    });

    return Member;
}
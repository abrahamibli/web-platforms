const express = require('express');
const controller = require('../controllers/actors');

const router = express.Router();

router.get('/', controller.list);

router.get('/:id', controller.index);

router.put('/:id', controller.update);

router.patch('/:id', controller.replace);

router.delete('/:id', controller.destroy);

router.post('/', controller.create);

module.exports = router;
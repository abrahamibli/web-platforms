const express = require('express');
const controller = require('../controllers/dates');

const router = express.Router();

router.post('/', controller.getDate);

module.exports = router;
const Sequelize = require('sequelize');

const directorModel = require('./models/director');
const genreModel = require('./models/genre');
const movieModel = require('./models/movie');
const copyModel = require('./models/copy');
const memberModel = require('./models/member');
const bookingModel = require('./models/booking');
const actorModel = require('./models/actor');

// db's name, user, password, config
const sequelize = new Sequelize('vc', 'root', 'abcd123', {
    host: 'localhost',
    dialect: 'mysql'
});

const Director = directorModel(sequelize, Sequelize);
const Genre = genreModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const Copy = copyModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);
const Actor = actorModel(sequelize, Sequelize);

Director.hasMany(Movie, { as: 'movie', foreignKey: 'director_id' });
Genre.hasMany(Movie, { as: 'movie', foreignKey: 'genre_id' });
Movie.hasMany(Copy, { as: 'copy', foreignKey: 'movie_id' });
Copy.hasMany(Booking, { as: 'booking', foreignKey: 'copy_id' });
Member.hasMany(Booking, { as: 'booking', foreignKey: 'member_id' });
Actor.belongsToMany(Movie, { through: 'MOVIES_ACTORS', foreignKey: 'actor_id' });
Movie.belongsToMany(Actor, { through: 'MOVIES_ACTORS', foreignKey: 'movie_id' });

sequelize.sync({
    force: true
}).then(() => {
    console.log("DB create");
});

module.exports = {
    Director,
    Genre,
    Movie,
    Copy,
    Member,
    Booking,
    Actor
};